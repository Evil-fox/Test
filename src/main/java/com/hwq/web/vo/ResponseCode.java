package com.hwq.web.vo;

/**
 * Created by Fox on 2017/5/30.
 */
public enum ResponseCode {

    SUCCESS(0,"success"),
    ERROR(1,"ERROR");

    private final int code;
    private final String msg;

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    ResponseCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
