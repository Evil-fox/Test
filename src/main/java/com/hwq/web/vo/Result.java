package com.hwq.web.vo;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.Serializable;

@JsonSerialize(include =  JsonSerialize.Inclusion.NON_NULL)
public class Result<T> implements Serializable {


	    // 状态.0表示正常
	    private int code;
	    
	    // 处理失败时,返回消息,否则返回空
	    private String msg;
	    
	    // 处理正常时,返回结果,返回返回空
	    private T data;

	public Result(int code, String msg, T data) {
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	public Result(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}


	public static <T> Result<T> createBySuccessMessage(String msg){
		return new Result<T>(ResponseCode.SUCCESS.getCode(),msg);
	}


	public static <T> Result<T> createBySuccessMessage(int code,String msg,T data){
		return  new Result<T>(ResponseCode.SUCCESS.getCode(),msg,data);
	}

	public static <T> Result<T> createByErrorMessage(String msg){
		return new Result<T>(ResponseCode.ERROR.getCode(),msg);
	}

	public static <T> Result<T> createByErrorCodeMessage(int code,String msg){
		return new Result<T>(code,msg);
	}
}
