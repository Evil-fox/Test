package com.hwq.web.dao;

import com.hwq.web.entity.BookStock;

public interface BookStockMapper {

    int updateBookStock(String isbn);

    BookStock findBookStockByid(String isbn);
}