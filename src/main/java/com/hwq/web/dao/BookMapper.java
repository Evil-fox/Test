package com.hwq.web.dao;

import org.springframework.stereotype.Repository;

@Repository
public interface BookMapper {

    int findBookById(String isbn);

}