package com.hwq.web.service.impl;

import com.hwq.web.dao.AccountMapper;
import com.hwq.web.dao.BookMapper;
import com.hwq.web.dao.BookStockMapper;
import com.hwq.web.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Fox on 2017/6/1.
 */
@Service
public class BookServiceImpl implements BookService{

    @Autowired
    private BookMapper bookMapper;
    @Autowired
    private BookStockMapper bookStockMapper;
    @Autowired
    private AccountMapper accountMapper;

    //根据书号获取书的单价
    public int findBookById(String ibsn) {

        return bookMapper.findBookById(ibsn);
    }

    //更新数据库库存，使书号对应的库存-1
    public void updateBookStock(String isbn) {

        bookStockMapper.updateBookStock(isbn);
    }

    //更新用户的账户余额，使得username的 balance - price
    public void updateUserAccount(String username, int price) {

        accountMapper.updateUserAccount(username,price);
    }

}
