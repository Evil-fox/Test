package com.hwq.web.service;

/**
 * Created by Fox on 2017/6/1.
 */
public interface BookService {

    //根据书号获取书的单价
    public int findBookById(String ibsn);

    //更新数据库库存，使书号对应的库存-1
    public void updateBookStock(String isbn);

    //更新用户的账户余额，使得username的 balance - price
    public void updateUserAccount(String username,int price);
}
