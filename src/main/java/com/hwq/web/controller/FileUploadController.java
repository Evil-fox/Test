package com.hwq.web.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;



@Controller
@RequestMapping("/upload")
public class FileUploadController{

    private SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");

//    @Autowired
//    private PersonDao personDao;
//
    /**
     * @方法名： uploadShaidanPhoto
     * @功能描述：上传多张资质图片
     */
    @RequestMapping(value="/uploadQualifications", method=RequestMethod.POST)
    @ResponseBody
    public void uploadQualification(HttpServletRequest request,
                                      @RequestParam(value="file", required=true) MultipartFile[] file) throws Exception {

        StringBuffer sb = new StringBuffer();
//        Person person = new Person();
        for(MultipartFile item :file){
            String filename = item.getOriginalFilename();
            // TODO 上传文件格式过滤
            String filePath = getFilePath("picture", filename);
            String realPath = "/Users/Macx/Desktop/image/";  //自己电脑上新建目录写这类
            File targetFile = new File(realPath + filePath);
            // 目录不存在时，要手动创建目录，该代码在当天首次上传图片时会执行
            File directory = targetFile.getParentFile();
            if (!directory.exists()) {
                directory.mkdirs();
            }
            if(item.getSize() > 0 ){
                item.transferTo(targetFile); //转存文件到指定目录下
                sb.append(filePath).append(",");
            }
        }
//        person.setImg(sb.deleteCharAt(sb.length()-1).toString());
//        person.setUserId(userId);
//        int num = loadUserMapper.update(person);
//        if(num < 0){
//            return ServerResponse.createBySuccessMessage("更新失败");
//        }
//
//        return ServerResponse.createBySuccessMessage("更新成功");

    }
    
    

    private String getFilePath(String path, String filename) {
        return path + "/" + getDirname() + "/"  + filename;
    }
    
    private String getDirname() {
        return format.format(new Date());
    }
    
}
