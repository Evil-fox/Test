package com.hwq.web.controller;

import com.hwq.web.service.BookService;
import com.hwq.web.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Fox on 2017/6/1.
 */
@Controller
@RequestMapping(value = "/book")
public class BookController {

    @Autowired
    private BookService bookService;

    @RequestMapping(value = "/index")
    public @ResponseBody Integer toIdex(){

        return bookService.findBookById("1001");
    }
}
