package com.hwq.web.service.impl;

import com.hwq.web.dao.AccountMapper;
import com.hwq.web.dao.BookMapper;
import com.hwq.web.dao.BookStockMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Created by Fox on 2017/6/1.
 */
public class BookServiceImplTest {

    @Autowired
    private BookMapper bookMapper;
    @Autowired
    private BookStockMapper bookStockMapper;
    @Autowired
    private AccountMapper accountMapper;

    @Test
    public void findBookById() {
        System.out.println(bookMapper.findBookById("1001"));
    }

    @Test
    public void updateBookStock() {
    }

    @Test
    public void updateUserAccount() {
    }

}